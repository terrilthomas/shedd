package com.shedd;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        SearchView.OnQueryTextListener, SearchView.OnCloseListener,
        Response.ErrorListener, SearchView.OnSuggestionListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private List<DataModel> mImgurDataList = new ArrayList<>();
    private List<DataModel> mPixbayDataList = new ArrayList<>();
    private List<DataModel> mDataList = new ArrayList<>();
    private List<DataModel> mDataListCopy = new ArrayList<>();
    private ProgressDialog mDialog;
    private SocialMediaAdapater mMediaAdapter;
    private final String PARCED_DATA = "parcedData";
    public final String IMAGEURL_PREFIX = "http://i.imgur.com/";
    public final String IMAGEURL_POSTFIX = "m.jpg";
    private RequestQueue mQueue;
    private SearchRecentSuggestions mSuggestions;
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mQueue = VolleyRequestQueue.getInstance(this)
                .getRequestQueue();
        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) findViewById(R.id.recylerView);
        GridLayoutManager layoutManager = new GridLayoutManager(getBaseContext(), 2);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        mMediaAdapter = new SocialMediaAdapater();
        mRecyclerView.setAdapter(mMediaAdapter);

        if (savedInstanceState == null) {

            mDialog = new ProgressDialog(MainActivity.this);
            mDialog.setTitle("Loading ...");
            mDialog.show();

            callImgurAPI();
            callPixBayAPI();
        }
    }

    private void callImgurAPI() {
        final VolleyJSONObjectRequest jsonRequest = new VolleyJSONObjectRequest(Request.Method
                .GET, Commons.IMGUR_URL,
                new JSONObject(), getResponse(ServiceType.IMGURAPI), this);
        jsonRequest.setTag(TAG);
        mQueue.add(jsonRequest);
    }

    private void callPixBayAPI() {
        final VolleyJSONObjectRequest jsonRequest = new VolleyJSONObjectRequest(Request.Method
                .GET, Commons.PIXBAY_URL,
                new JSONObject(), getResponse(ServiceType.PIXBAYAPI), this);
        jsonRequest.setTag(TAG);
        mQueue.add(jsonRequest);
    }

    private void handleSearchHistory(String query) {
        mSuggestions = new SearchRecentSuggestions(this,
                SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);
        mSuggestions.saveRecentQuery(query, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        mSearchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        return true;
    }

    private void doSearch(String newText) {
        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        String contentUri = "content://" + SuggestionProvider.AUTHORITY + '/' + SearchManager.SUGGEST_URI_PATH_QUERY;
        Uri uri = Uri.parse(contentUri);
        Cursor cursor = contentResolver.query(uri, null, null, new String[]{newText}, null);

        cursor.moveToFirst();
        String[] columns = new String[]{SearchManager.SUGGEST_COLUMN_TEXT_1};
        int[] views = new int[]{R.id.tv_search_result};
        CursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.view_search, cursor, columns, views, 0);
        mSearchView.setSuggestionsAdapter(adapter);
        mSearchView.setOnSuggestionListener(this);
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        handleSearchHistory(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        doSearch(newText);
        mMediaAdapter.filter(newText);
        return true;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        Log.e(TAG, error.getMessage());
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return false;
    }

    @Override
    public boolean onSuggestionClick(int position) {
        Cursor cursor = mSearchView.getSuggestionsAdapter().getCursor();
        cursor.moveToPosition(position);
        String selectedText = cursor.getString(2);//2 is the index of col containing suggestion name.
        mSearchView.setQuery(selectedText, false);
        return true;
    }

    private class SocialMediaAdapater extends RecyclerView.Adapter<SocialMediaAdapater.ViewHolder> {


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.view_adapter, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Glide.with(getBaseContext())
                    .load(mDataList.get(position).getImageUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .crossFade()
                    .into(holder.mGalleryImage);
            holder.mGalleryName.setText(mDataList.get(position).getTitle());
        }

        @Override
        public int getItemCount() {
            return mDataList.size();
        }

        public void filter(String text) {
            mDataList.clear();
            if (text.isEmpty()) {
                mDataList.addAll(mDataListCopy);
            } else {
                text = text.toLowerCase();
                for (DataModel item : mDataListCopy) {
                    if (item.getTitle().toLowerCase().contains(text)) {
                        mDataList.add(item);
                    }
                }
            }
            notifyDataSetChanged();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView mGalleryImage;
            TextView mGalleryName;

            public ViewHolder(View itemView) {
                super(itemView);
                mGalleryImage = (ImageView) itemView.findViewById(R.id.imv_Gallery);
                mGalleryName = (TextView) itemView.findViewById(R.id.tv_Gallery);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(PARCED_DATA, (ArrayList) mDataList);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mDataList = savedInstanceState.getParcelableArrayList(PARCED_DATA);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mQueue != null) {
            mQueue.cancelAll(TAG);
        }
    }

    private Response.Listener getResponse(final ServiceType type) {
        Response.Listener listener = new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                mDialog.dismiss();
                JSONObject object = (JSONObject) response;
                if (type == ServiceType.IMGURAPI) {
                    try {
                        JSONArray array = object.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject responseObject = array.getJSONObject(i);
                            DataModel model = new DataModel();
                            model.setId(responseObject.getString("id"));
                            model.setTitle(responseObject.getString("title"));
                            model.setImageUrl(IMAGEURL_PREFIX + responseObject.getString("id") + IMAGEURL_POSTFIX);
                            mImgurDataList.add(model);
                        }
                        mDataList.addAll(mImgurDataList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (type == ServiceType.PIXBAYAPI) {
                    try {
                        JSONArray array = object.getJSONArray("hits");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject responseObject = array.getJSONObject(i);
                            DataModel model = new DataModel();
                            model.setId(responseObject.getString("id"));
                            model.setTitle(responseObject.getString("tags"));
                            model.setImageUrl(responseObject.getString("previewURL"));
                            mPixbayDataList.add(model);
                        }
                        mDataList.addAll(mPixbayDataList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                mDataListCopy.addAll(mDataList);
                mMediaAdapter.notifyDataSetChanged();
            }
        };
        return listener;
    }

    @Override
    protected void onDestroy() {
        if (mSuggestions != null) {
            mSuggestions.clearHistory();
        }
        super.onDestroy();
    }

    enum ServiceType {
        IMGURAPI,
        PIXBAYAPI;
    }
}
