package com.shedd;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Terril-Den on 5/7/17.
 */

public class SuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY =  "com.shedd.SuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public SuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
