package com.shedd;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Terril-Den on 5/11/17.
 */

public class DataModel implements Parcelable{

    private String id;
    private String title;
    private String imageUrl;

    public DataModel(){

    }
    protected DataModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<DataModel> CREATOR = new Creator<DataModel>() {
        @Override
        public DataModel createFromParcel(Parcel in) {
            return new DataModel(in);
        }

        @Override
        public DataModel[] newArray(int size) {
            return new DataModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(imageUrl);
    }
}
