package com.shedd;

/**
 * Created by Terril-Den on 5/7/17.
 */

public class Commons {
    public static final String PIXBAY_URL_PREFIX = "https://pixabay.com/api/?key=";
    public static final String PIXBAY_URL_POSTFIX = "&image_type=photo&pretty=true";
    public static final String PIXBAY_KEY = "5299282-dbf335ff9f9f8e06364f2eeca";

    public static final String PIXBAY_URL = PIXBAY_URL_PREFIX + PIXBAY_KEY + PIXBAY_URL_POSTFIX;

    public static final String IMGUR_URL = "https://api.imgur.com/3/gallery/hot/viral/0";
    public static final String IMGUR_CLIENT_ID = "14d95181d8b12f3";
    public static final String IMGUR_CLIENT_SECRET = "47b2dab8de7a80599fff9c07b39f425fd5e4a8b7";

}
