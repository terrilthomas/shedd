package com.shedd;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Test Case to check API call
 */
@RunWith(AndroidJUnit4.class)
public class SheddTest extends AndroidTestCase {
    Context appContext;

    @Before
    public void useAppContext() {
        // Context of the app under test.
        appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.shedd", appContext.getPackageName());
    }

    @Test
    public void testImgurAPI() throws Exception {

        RequestQueue mQueue = VolleyRequestQueue.getInstance(appContext)
                .getRequestQueue();
        final VolleyJSONObjectRequest jsonRequest = new VolleyJSONObjectRequest(Request.Method
                .GET, Commons.IMGUR_URL,
                new JSONObject(), new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                assertNotNull(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                assertFalse(error.getMessage(), true);
            }
        });
        mQueue.add(jsonRequest);
    }


    @Test
    public void testPixbayAPI() throws Exception {

        RequestQueue mQueue = VolleyRequestQueue.getInstance(appContext)
                .getRequestQueue();
        final VolleyJSONObjectRequest jsonRequest = new VolleyJSONObjectRequest(Request.Method
                .GET, Commons.PIXBAY_URL,
                new JSONObject(), new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                assertNotNull(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                assertFalse(error.getMessage(), true);
            }
        });
        mQueue.add(jsonRequest);
    }
}
